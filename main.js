let state = {
    currentRound: 0,
    results: [],
    gameInProgress: false,
};

const getRandomFrequency = () => Math.floor(Math.random() * 40) + 1;

const startGame = () => {
    state = { currentRound: 1, gameInProgress: true, results: [] };
    document.getElementById("resultsArea").innerHTML = '';
    displayFlashcard(getRandomFrequency());
};

const recordGuess = (actualFrequency, guessedFrequency, flashcard) => {
    state.results.push({ actualFrequency, guessedFrequency });
    flashcard.textContent = `Actual frequency: ${actualFrequency} Hz`;
    flashcard.style.color = Math.abs((guessedFrequency - actualFrequency) / actualFrequency) <= 0.1 ? 'green' : 'red';
    setTimeout(() => {
        if (state.currentRound < 20) {
            state.currentRound++;
            displayFlashcard(getRandomFrequency());
        } else {
            state.gameInProgress = false;
            displayResults();
        }
    }, 2000);
};

const getColorSettings = () => ({
    hue: parseInt(document.getElementById("hue").value),
    saturation: parseInt(document.getElementById("saturation").value),
    brightness: parseInt(document.getElementById("brightness").value),
    hueFlashIntensity: parseInt(document.getElementById("hueFlashIntensity").value),
    saturationFlashIntensity: parseInt(document.getElementById("saturationFlashIntensity").value),
    brightnessFlashIntensity: parseInt(document.getElementById("brightnessFlashIntensity").value),
});

const getColor = (baseColor, flashIntensity, flash) => {
    const color = baseColor + flashIntensity * (flash ? 1 : -1);
    return Math.min(Math.max(color, 0), 100);
};

const displayFlashcard = (frequency) => {
    const gameArea = document.getElementById('gameArea');
    gameArea.innerHTML = '';

    const flashcard = document.createElement('div');
    flashcard.className = 'flashcard';

    const input = document.createElement('input');
    input.type = 'number';
    input.addEventListener("keyup", function(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            recordGuess(frequency, parseInt(input.value), flashcard);
            input.focus
            event.preventDefault();
            recordGuess(frequency, parseInt(input.value), flashcard);
            input.focus();
        }
    });

    const submitButton = document.createElement('button');
    submitButton.textContent = 'Submit Guess';
    submitButton.onclick = () => {
        recordGuess(frequency, parseInt(input.value), flashcard);
        input.focus();
    };

    gameArea.appendChild(flashcard);
    gameArea.appendChild(input);
    gameArea.appendChild(submitButton);

    let flashing = true;
    setInterval(() => {
        const { hue, saturation, brightness, hueFlashIntensity, saturationFlashIntensity, brightnessFlashIntensity } = getColorSettings();
        const newHue = getColor(hue, hueFlashIntensity, flashing);
        const newSaturation = getColor(saturation, saturationFlashIntensity, flashing);
        const newBrightness = getColor(brightness, brightnessFlashIntensity, flashing);
        flashcard.style.backgroundColor = `hsl(${newHue}, ${newSaturation}%, ${newBrightness}%)`;
        flashing = !flashing;
    }, 1000 / frequency);
};


const displayResults = () => {
    const totalError = state.results.reduce((sum, result) => sum + Math.abs(result.actualFrequency - result.guessedFrequency), 0);
    const averageError = totalError / state.results.length;

    const message = document.createElement('p');
    message.textContent = `Game over! Your average error was ${averageError.toFixed(2)} Hz.`;
    document.getElementById("resultsArea").appendChild(message);

    displayFrequencySpectrumGraph(state.results);
};

const displayFrequencySpectrumGraph = (results) => {
    let frequencyData = Array(40).fill(0);
    let frequencyCount = Array(40).fill(0);

    results.forEach(({ actualFrequency, guessedFrequency }) => {
        frequencyData[actualFrequency - 1] += Math.abs(actualFrequency - guessedFrequency);
        frequencyCount[actualFrequency - 1]++;
    });

    let chartData = [['Frequency', 'Average Error']];
    for (let i = 0; i < 40; i++) {
        if (frequencyCount[i] > 0) {
            chartData.push([i + 1, frequencyData[i] / frequencyCount[i]]);
        }
    }

    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(() => {
        var data = google.visualization.arrayToDataTable(chartData);

        var options = {
            title: 'Frequency Spectrum',
            hAxis: { title: 'Frequency (Hz)' },
            vAxis: { title: 'Average Error (Hz)' },
            legend: 'none'
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    });
};

document.getElementById("startButton").onclick = startGame;

